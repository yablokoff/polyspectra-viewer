import { releaseProxy, wrap } from '../libs/comlink.js';

// const DigifabsterIframeSrc = 'https://app.digifabster.com/polyspectra/widget/upload/';
const DigifabsterIframeSrc = 'https://app-test.digifabster.com/4taps/widget/upload';

export class ClientExposedAPI {
    constructor() {
        this.events = {};
        this.url = new URL(DigifabsterIframeSrc);

        this.handleDocumentReady = this.handleDocumentReady.bind(this);
        this.handleInitMessage = this.handleInitMessage.bind(this);

        document.addEventListener('DOMContentLoaded', this.handleDocumentReady);
    }

    emit(event, ...args) {
        ;(this.events[event] || []).forEach((i) => i(...args))
    }

    on(event, cb) {
        ;(this.events[event] = this.events[event] || []).push(cb)
        return () =>
            (this.events[event] = (this.events[event] || []).filter(
                (l) => l !== cb
            ))
    }

    handleInitMessage(event) {
        if (!this.eventIsValid(event)) return;

        this.setupApi(event.ports[0]);
    }

    eventIsValid(event) {
        return Boolean(event.origin.startsWith(this.url.origin) && event.data.status === 'ready' && event.ports[0]);
    }

    setupApi(port) {
        this.port = port;
        this.api = wrap(port);
        this.emit('ready', this.api);
    }

    handleDocumentReady() {
        const that = this;

        const script = document.querySelector(`script[src="https://digifabster.com/static/js/widget.js"]`);
        script && script.addEventListener(
            'load',
            function () {
                const iframe = document.querySelector(`iframe[src^="${window.__digifabsterCompanyWidgetUrl}"]`);
                iframe && iframe.addEventListener(
                    'load',
                    function () {
                        that.init();
                    },
                    { once: true },
                );
            },
            { once: true },
        );
    }

    init() {
        // Listen for the initial port transfer message
        window.addEventListener('message', this.handleInitMessage);
    }

    destroy() {
        // console.log('destroy');
        window.removeEventListener('message', this.handleInitMessage);
        this.api && this.api[releaseProxy]();
    }

    call(name, ...args) {
        const that = this;
        return new Promise(function(resolve) {
            if (that.ready) {
                that.api[name](...args).then(function (res) {
                    resolve(res);
                })
            } else {
                const unsubscribe = that.on('ready', function () {
                    unsubscribe();

                    that.api[name](...args).then(function (res) {
                        resolve(res);
                    })
                });
            }
        })
    }

    get ready() {
        return Boolean(this.port && this.api);
    }
}

window.digifabsterExposedApi = new ClientExposedAPI();
