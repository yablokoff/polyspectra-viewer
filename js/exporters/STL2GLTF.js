let GLOBAL = {
  color: [
    150,
    150,
    150,
    1
  ],
  get color_str() {
    return `rgba(${ this.color[0] },
            
    ${ this.color[1] },
            
    ${ this.color[2] },
            
    ${ this.color[3] })`;
  },
  get color_0to1() {
    return [this.color[0] / 255,
      this.color[1] / 255,
      this.color[2] / 255,
      this.color[3]];
  },
  stl_name: undefined
};
let last_uploaded_file = null;

function uploaded(file) {
  if (file === undefined) {
    var uploadform = document.getElementById('fileuploadform');
    file = uploadform.files[0];
    this.value = null;
  }
  check_file(file, function () {
    check_file_success()
  });
  function check_file_success() {
    last_uploaded_file = file;

    if (file.name.indexOf('.glb') >= 0 || file.name.indexOf('.GLB') >= 0) {
      var filename = file.name;
      var fr = new FileReader();
      fr.readAsArrayBuffer(file);
      fr.onload = function () {
        parseGLB(fr.result)
      }
      return;
    }
    put_status('Converting .STL locally in your Browser...');
    var uploadform = document.getElementById('fileuploadform');
    var filename = file.name;
    var fr = new FileReader();
    fr.readAsDataURL(file);
    fr.onload = function () {
      console.log(filename);
      if (filename === GLOBAL.stl_name) {
        console.log('same stl file');
        download_glb();
        return;
      } else if (GLOBAL.stl_name !== undefined) {
        console.log('new filename ', filename, 'unlink', GLOBAL.stl_name);
        Module['FS_unlink'](GLOBAL.stl_name);
      }
      var stl_name = filename;
      var data = atob(fr.result.split(',') [1]);
      Module['FS_createDataFile']('.', stl_name, data, true, true);
      Module.ccall('make_bin', undefined, [
        'string'
      ], [
        stl_name
      ]);
      let out_data = Module['FS_readFile']('data.txt', {
        encoding: 'utf8'
      });
      out_data = out_data.split(' ');
      GLOBAL.number_indices = parseInt(out_data[0]);
      GLOBAL.number_vertices = parseInt(out_data[1]);
      GLOBAL.indices_blength = parseInt(out_data[2]);
      GLOBAL.vertices_blength = parseInt(out_data[3]);
      GLOBAL.total_blength = parseInt(out_data[4]);
      GLOBAL.minx = parseFloat(out_data[5]);
      GLOBAL.miny = parseFloat(out_data[6]);
      GLOBAL.minz = parseFloat(out_data[7]);
      GLOBAL.maxx = parseFloat(out_data[8]);
      GLOBAL.maxy = parseFloat(out_data[9]);
      GLOBAL.maxz = parseFloat(out_data[10]);
      GLOBAL.stl_name = stl_name;
      download_glb();
    }
  }
}
function gltf_dict(total_blength, indices_blength, vertices_boffset, vertices_blength, number_indices, number_vertices, minx, miny, minz, maxx, maxy, maxz, color_r, color_g, color_b) {
  return {
    'extensionsUsed': [
      'KHR_texture_transform',
      'KHR_materials_clearcoat',
      'KHR_materials_transmission',
      'KHR_materials_sheen',
      'KHR_materials_volume'
    ],
    'scenes': [
      {
        'nodes': [
          0
        ]
      }
    ],
    'nodes': [
      {
        'mesh': 0,
        'rotation': [
          - 0.70710678119,
          0,
          0,
          0.70710678119
        ]
      }
    ],
    'meshes': [
      {
        'primitives': [
          {
            'attributes': {
              'POSITION': 1
            },
            'indices': 0,
            'material': 0
          }
        ]
      }
    ],
    'buffers': [
      {
        'byteLength': total_blength
      }
    ],
    'bufferViews': [
      {
        'buffer': 0,
        'byteOffset': 0,
        'byteLength': indices_blength,
        'target': 34963
      },
      {
        'buffer': 0,
        'byteOffset': vertices_boffset,
        'byteLength': vertices_blength,
        'target': 34962
      }
    ],
    'accessors': [
      {
        'bufferView': 0,
        'byteOffset': 0,
        'componentType': 5125,
        'count': number_indices,
        'type': 'SCALAR',
        'max': [
          number_vertices - 1
        ],
        'min': [
          0
        ]
      },
      {
        'bufferView': 1,
        'byteOffset': 0,
        'componentType': 5126,
        'count': number_vertices,
        'type': 'VEC3',
        'min': [
          minx,
          miny,
          minz
        ],
        'max': [
          maxx,
          maxy,
          maxz
        ]
      }
    ],
    'asset': {
      'version': '2.0',
      'generator': 'STL2GLTF'
    },
    'materials': [
      {
        'name': 'COR Alpha',
        'pbrMetallicRoughness': {
          'baseColorFactor': [
            0.87,
            0.71,
            0.04,
            1
          ],
          'metallicFactor': 0.1,
          'roughnessFactor': 0.25
        },
        'alphaMode': 'OPAQUE',
        'alphaCutoff': 0.5,
        'doubleSided': true,
        'extensions': {
          'KHR_materials_transmission': {
            'transmissionFactor': 1
          },
          'KHR_materials_ior': {
            'ior': 1.55,
          },
          'KHR_materials_volume': {
            'thicknessFactor': 3,
            'attenuationDistance': 3,
            'attenuationColor': [
              1,
              1,
              1
            ]
          }
        }
      }
    ]
  }
}
async function download_glb() {
  if (GLOBAL.stl_name === undefined) {
    put_status('Please upload a file');
    return;
  } else {
  }
  const stl_name = GLOBAL.stl_name;
  const color = GLOBAL.color_0to1;
  const total_blength = GLOBAL.total_blength;
  const indices_blength = GLOBAL.indices_blength;
  const vertices_boffset = GLOBAL.indices_blength;
  const vertices_blength = GLOBAL.vertices_blength;
  const number_indices = GLOBAL.number_indices;
  const number_vertices = GLOBAL.number_vertices;
  const minx = GLOBAL.minx;
  const miny = GLOBAL.miny;
  const minz = GLOBAL.minz;
  const maxx = GLOBAL.maxx;
  const maxy = GLOBAL.maxy;
  const maxz = GLOBAL.maxz;
  const gltf_json = JSON.stringify(gltf_dict(total_blength, indices_blength, vertices_boffset, vertices_blength, number_indices, number_vertices, minx, miny, minz, maxx, maxy, maxz, color[0], color[1], color[2]));
  const out_bin_bytelength = total_blength;
  const header_bytelength = 20;
  const scene_len = gltf_json.length;
  const padded_scene_len = ((scene_len + 3) & ~3);
  const body_offset = padded_scene_len + header_bytelength;
  const file_no_bin_len = body_offset + 8;
  const file_len = file_no_bin_len + out_bin_bytelength;
  let glb = new Uint8Array(file_no_bin_len);
  glb[0] = 103;
  glb[1] = 108;
  glb[2] = 84;
  glb[3] = 70;
  glb[4] = (2) & 255;
  glb[5] = (2 >> 8) & 255;
  glb[6] = (2 >> 16) & 255;
  glb[7] = (2 >> 24) & 255;
  glb[8] = (file_len) & 255;
  glb[9] = (file_len >> 8) & 255;
  glb[10] = (file_len >> 16) & 255;
  glb[11] = (file_len >> 24) & 255;
  glb[12] = (padded_scene_len) & 255;
  glb[13] = (padded_scene_len >> 8) & 255;
  glb[14] = (padded_scene_len >> 16) & 255;
  glb[15] = (padded_scene_len >> 24) & 255;
  glb[16] = 74;
  glb[17] = 83;
  glb[18] = 79;
  glb[19] = 78;
  for (let i = 0; i < gltf_json.length; i++) {
    glb[i + header_bytelength] = gltf_json.charCodeAt(i);
  }
  for (let i = 0; i < padded_scene_len - scene_len; i++) {
    glb[i + scene_len + header_bytelength] = 32;
  }
  glb[body_offset] = (out_bin_bytelength) & 255;
  glb[body_offset + 1] = (out_bin_bytelength >> 8) & 255;
  glb[body_offset + 2] = (out_bin_bytelength >> 16) & 255;
  glb[body_offset + 3] = (out_bin_bytelength >> 24) & 255;
  glb[body_offset + 4] = 66;
  glb[body_offset + 5] = 73;
  glb[body_offset + 6] = 78;
  glb[body_offset + 7] = 0;
  let out_bin = Module['FS_readFile']('out.bin');
  let blob = new Blob([glb,
    out_bin], {
    type: 'application/sla'
  });
  const glb_name = stl_name.slice(0, stl_name.length - 4) + '.glb';
  var buffer = await blob.arrayBuffer();
  parseGLB(buffer);
  put_status('This is what your part will look like in COR Alpha. Ready to print? Click below.')
}
function check_file(file, success_cb) {
  put_status('Checking file');
  const filename = file.name;
  const extension = filename.toLowerCase().slice(filename.lastIndexOf('.') + 1, filename.length);
  if (extension !== 'stl') {
    put_status(extension + ' file found');
  }
  success_cb();
}
function dodrop(event) {
  var dt = event.dataTransfer;
  var file = dt.files[0];
  var filename = file.name;
  put_status('Converting ' + filename + ' to GLB using WebAssembly.');
  uploaded(file);
}
function put_status(text) {
  document.getElementById('status').textContent = text;
}

window.addEventListener('load', function () {
  const df_widget_btn = document.getElementById('df-widget-btn');
  if (!df_widget_btn) return;

  df_widget_btn.addEventListener('click', function() {
    if (last_uploaded_file) {
      window.digifabsterExposedApi.call('transferModels', [last_uploaded_file]).then(function () {
        last_uploaded_file = null;
      });
    }
  });
});
