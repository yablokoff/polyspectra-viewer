import * as THREE from'./libs/three.module.js';
import {
  OrbitControls
}
from'./libs/controls/OrbitControls.js';
import {
  GLTFLoader
}
from'./libs/loaders/GLTFLoader.js';
import {
  DRACOLoader
}
from'./libs/loaders/DRACOLoader.js';
import {
  RGBELoader
}
from'./libs/loaders/RGBELoader.js';
let orbitControls;
let camera,
scene,
renderer,
loader;
let target = new THREE.Group();
;
const clock = new THREE.Clock();
const settings = [
  {
    name: 'Transmission',
    cameraPos: new THREE.Vector3(0, 0, 1),
    objectRotation: new THREE.Euler(0, 0, 0),
    envmap: 'textures/equirectangular/footprint_court.hdr',
    background: 'textures/equirectangular/footprint_court_blur.jpg',
    isTest: false,
    attenuationDistance: undefined,
  }
];
function initRender() {
  renderer = new THREE.WebGLRenderer({
    antialias: true
  });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.toneMapping = THREE.ACESFilmicToneMapping;
  renderer.toneMappingExposure = 1;
  renderer.outputEncoding = THREE.sRGBEncoding;
  document.getElementById('container').appendChild(renderer.domElement);
  camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 100);
  orbitControls = new OrbitControls(camera, renderer.domElement);
  orbitControls.addEventListener('change', render);
  onWindowResize();
  window.addEventListener('resize', onWindowResize);
}
function setBackground(texture) {
  var pmremGenerator = new THREE.PMREMGenerator(renderer);
  pmremGenerator.compileEquirectangularShader();
  let background = pmremGenerator.fromEquirectangular(texture).texture;
  texture.dispose();
  pmremGenerator.dispose();
  scene.background = background;
  render();
}
function setEnv(texture) {
  if (isMobile()) return;
  var pmremGenerator = new THREE.PMREMGenerator(renderer);
  pmremGenerator.compileEquirectangularShader();
  let background = pmremGenerator.fromEquirectangular(texture).texture;
  texture.dispose();
  pmremGenerator.dispose();
  scene.environment = background;
  render();
}
function initScene(sceneInfo) {
  scene = new THREE.Scene();
  scene.add(target);
  if (sceneInfo.cameraPos) {
    camera.position.copy(sceneInfo.cameraPos);
  }
  if (sceneInfo.envmap) {
    if (sceneInfo.envmap.indexOf('.hdr') >= 0) {
      new RGBELoader().setDataType(THREE.UnsignedByteType).load(sceneInfo.envmap, function (texture) {
        setEnv(texture)
      });
    } else {
      new THREE.TextureLoader().load(sceneInfo.envmap, map=>{
        setEnv(map);
      })
    }
  }
  if (sceneInfo.background) {
    if (sceneInfo.background.indexOf('.hdr') >= 0) {
      new RGBELoader().setDataType(THREE.UnsignedByteType).load(sceneInfo.background, function (texture) {
        setBackground(texture)
      });
    } else {
      new THREE.TextureLoader().load(sceneInfo.background, map=>{
        setBackground(map);
      })
    }
  }
  loader = new GLTFLoader();
  const dracoLoader = new DRACOLoader();
  dracoLoader.setDecoderPath('js/libs/draco/gltf/');
  loader.setDRACOLoader(dracoLoader);
  window.parseGLB = function (buffer) {
    const loadStartTime = performance.now();
    loader.parse(buffer, '', function (data) {
      let gltf = data;
      const object = gltf.scene.children[gltf.scene.children.length - 1];
      console.info('Load time: ' + (performance.now() - loadStartTime).toFixed(2) + ' ms.');
      let box = new THREE.Box3();
      box.setFromObject(object);
      let center = new THREE.Vector3();
      let size = new THREE.Vector3();
      box.getCenter(center);
      box.getSize(size);
      let bb = Math.max(size.x, Math.max(size.y, size.z));
      object.position.sub(center);
      console.log('Object bb size ' + bb);
      orbitControls.reset();
      camera.position.z = bb;
      camera.far = bb * 3;
      camera.updateProjectionMatrix();
      orbitControls.minDistance = 0;
      orbitControls.maxDistance = bb * 2;
      orbitControls.enableDamping = true;
      target.remove(...target.children)
      target.add(object);
      target.traverse(child=>{
        if (child.material) {
          child.material.side = THREE.DoubleSide;
        }
      })
      render();
    }, undefined, function (error) {
      console.error(error);
    });
  }
}
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize(window.innerWidth, window.innerHeight);
}
function animate() {
  requestAnimationFrame(animate);
  orbitControls.update();
  render();
}
function render() {
  renderer.render(scene, camera);
}
function onload() {
  initRender();
  initScene(settings[0]);
  setupMobile();
}
function isMobile() {
  return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
function setupMobile() {
  if (isMobile()) {
    console.log('Mobile');
  } else {
    console.log('Desktop');
  }
}
window.onload = onload;
